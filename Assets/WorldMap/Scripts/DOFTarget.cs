﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class DOFTarget : MonoBehaviour {

	DepthOfField DOF;
	public SelectCountryByClick CountryScript;
	// Use this for initialization
	void Start () {
		DOF = GetComponent<DepthOfField> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (CountryScript.CurrentCountry == null)
			return;
		DOF.focalTransform = CountryScript.CurrentCountry.transform;
	}
}
