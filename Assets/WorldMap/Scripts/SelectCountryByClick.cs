﻿using UnityEngine;
using System.Collections;

public class SelectCountryByClick : MonoBehaviour
{
    public Material Mat;
    public float AnimationSpeed = 0.1f;
    public float Offset = 0.3f;
    public float CamAnimSpeed = 1;
	public float CameraDistance = 10;
	public GameObject MoveVector;
	public Texture2D[] Flags;
	public GameObject CurrentCountry;

    private Vector3 oldPos;
    private Transform oldT;
    private Material oldMaterial;
    private float startAnimTime;
	private GUIStyle guiStyleHeader = new GUIStyle();

	void Start () {
	    
	}
	
    private void Update()
    {
        SelectCounty();
        SelectedCountyAnimationMove();
        MoveCameraToCounty();

    }

	private Texture2D GetTextureByName(string name)
	{
		foreach (var flag in Flags) 
		{
			if (flag.name == name) return flag;
		}
		return null;
	}

    private void SelectCounty()
    {
        if (Input.GetMouseButtonDown(0)) {
            if (oldT!=null) {
                oldT.position = oldPos;
				oldT.GetComponent<Renderer>().sharedMaterial = oldMaterial;
            }
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                oldPos = hit.transform.position;
                oldT = hit.transform;
				var texture = GetTextureByName(hit.transform.name);
				oldMaterial = hit.transform.GetComponent<Renderer>().sharedMaterial;
				var material = hit.transform.GetComponent<Renderer>().material;
				material.SetTexture("_MainTex", texture);
				material.SetColor("_Color", Color.white);
				hit.transform.GetComponent<Renderer>().material = material;
				CurrentCountry = hit.transform.gameObject;
                startAnimTime = Time.time;
				distance = Vector3.zero;
            }
        }
    }

	void OnGUI(){
		if (oldT == null)
			return;
		GUI.Label(new Rect(300, 15, 100, 20), "Country name is \"" + oldT.name+"\"", guiStyleHeader);
	guiStyleHeader.fontSize = 30;
	guiStyleHeader.normal.textColor = new Color(1,1,1);}
	Vector3 distance;
    private void SelectedCountyAnimationMove()

    {
		distance += MoveVector.transform.forward * Time.deltaTime * AnimationSpeed;
		if (oldT!=null && distance.magnitude < Offset)
			oldT.position += MoveVector.transform.forward * Time.deltaTime * AnimationSpeed;
	}

    void MoveCameraToCounty()
    {
        var t = Camera.main.transform;
		var oldPosCam = t.localPosition;
        var pos = t.localPosition;
        pos.y = Mathf.SmoothStep(oldPosCam.y, oldPos.y, (Time.time - startAnimTime) / CamAnimSpeed);
        pos.z = Mathf.SmoothStep(oldPosCam.z, oldPos.z, (Time.time - startAnimTime) / CamAnimSpeed);
		if (oldT != null) {
			var distance = (t.position - oldT.position).magnitude;
			if ((distance - CameraDistance) > 0.5) {
				pos += t.forward*Time.deltaTime*3;
			}
			if ((distance - CameraDistance) < -0) {
				pos -= t.forward*Time.deltaTime*3;
			}

			Debug.Log (distance);
		}
			t.localPosition = pos;

    }
}
